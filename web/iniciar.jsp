<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html lang="en">
  <head>
    <%@ include file="layouts/head.jsp" %>
  </head>

  <body>
       <jsp:include page="layouts/nav.jsp" />
      <div class="container">
        <h2>Login</h2>

        <h3>Ingrese su usuario y contraseņa para poder realizar preguntas y respuestas:</h3>
        <c:if test="${param.error}">
            <div class="alert alert-danger">Datos incorrectos</div>
        </c:if>
        <form class="form" action="/electiva3/login" method="POST">
            <input type="text" name="user">
            <input type="password" name="password">
            <input type="submit" value="Enviar">
        </form>
      </div>
  </body>
</html>