<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<sql:query var="rs" dataSource="jdbc/TestDB">
	select id, texto from pregunta
</sql:query>

<html lang="en">
  <head>
    <%@ include file="layouts/head.jsp" %>
  </head>

  <body>

    <jsp:include page="layouts/nav.jsp" />
      
      <div class="container">
	  <h2>Formulario de respuesta</h2>

            <c:if test="${param.error}">
                <div class="alert alert-danger">Datos incorrectos</div>
            </c:if>
            <form class="form" action="/electiva3/respuesta" method="POST">
                <div class="form-group">
                    <label for="texto">Ingrese su respuesta</label>
                    <textarea class="form-control" id="texto" name="texto"></textarea>
                </div>
                <input type="hidden" name="pregunta_id" value="${param.id}">
                <button type="submit" class="btn btn-primary">Enviar</button>
            </form>
      </div>
  </body>
</html>