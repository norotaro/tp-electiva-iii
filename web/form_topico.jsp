<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<jsp:useBean id="topico" scope="request" class="politecnica.beans.Topico">
</jsp:useBean>

<html lang="en">
  <head>
    <%@ include file="layouts/head.jsp" %>
  </head>

  <body>

    <jsp:include page="layouts/nav.jsp" />
      
      <div class="container">
	  <h2>Asignar t�pico a pregunta</h2>

            <form class="form" action="/electiva3/topicos" method="POST">
                <input type="hidden" name="pregunta_id" value="${param.id}">
                <div class="form-group">
                    <label for="topico">Seleccione el t�pico</label>
                    <select class="form-control" name="topico_id" id="topico">
                        <c:forEach var="topico" items="${topico.findAll()}">
                            <option value="${topico.id}">${topico.nombre}</option>
                        </c:forEach>
                    </select>
                </div>
                <button type="submit" class="btn btn-primary">Enviar</button>
            </form>
      </div>
  </body>
</html>