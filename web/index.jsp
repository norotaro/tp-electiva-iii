<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<jsp:useBean id="pregunta" scope="request" class="politecnica.beans.Pregunta">
</jsp:useBean>

<html lang="en">
    <head>
        <%@ include file="layouts/head.jsp" %>
    </head>

    <body>
        <jsp:include page="layouts/nav.jsp" />
        <div class="container">
            <h2>Listado de Preguntas</h2>
            <c:if test="${sessionScope.usuario != null}"><a class="btn btn-primary" href="preguntas" role="button">Nueva Pregunta</a></c:if>

            <table class="table">
                <thead>
                    <tr>                      
                        <th>N�</th>
                        <th>Pregunta</th>
                        <th>Cant. de respuestas</th>
                        <th>T�picos</th>
                        <th>Opciones</th>
                    </tr>
                </thead>
                <c:forEach var="unaPregunta" items="${pregunta.findAll()}">
                    <tr>
                        <td>${unaPregunta.id}</td>                    
                        <td>${unaPregunta.texto}</td>
                        <td>${unaPregunta.cantRespuestas()}</td>
                        <c:choose>
                            <c:when test="${unaPregunta.topicos().size() > 0}"> 
                                <td>  
                                <c:forEach var="topico" items="${unaPregunta.topicos()}">
                                    <span class="label label-default">${topico.getNombre()}&nbsp&nbsp</span>
                                </c:forEach>
                                </td>
                            </c:when>                            
                            <c:otherwise>  <td></td> </c:otherwise>
                        </c:choose>                   
                        <td><a href="pregunta.jsp?id=${unaPregunta.id}">Ver</a></td>
                        </tr>
                </c:forEach>
            </table>
        </div>
        
        <footer>
            <%@ include file="layouts/footer.jsp" %>
        </footer>>
    
    </body>
</html>