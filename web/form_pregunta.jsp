<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html lang="en">
  <head>
    <%@ include file="layouts/head.jsp" %>
  </head>

  <body>

    <jsp:include page="layouts/nav.jsp" />
      
      <div class="container">
	  <h2>Formulario de pregunta</h2>

            <form class="form" action="/electiva3/preguntas" method="POST">
                <div class="form-group">
                    <label for="texto">Ingrese su pregunta:</label>
                    <textarea class="form-control" id="texto" name="texto"></textarea>
                </div>
                <button type="submit" class="btn btn-primary">Enviar</button>
            </form>
      </div>
  </body>
</html>