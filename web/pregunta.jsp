<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<sql:query var="rs" dataSource="jdbc/TestDB">
	select id, texto, usuario_id from pregunta where id=${param.id}
</sql:query>
        
<sql:query var="respuestas" dataSource="jdbc/TestDB">
	select resp.id, resp.texto, usu.nombre from respuesta as resp join usuario as usu on usu.id = resp.usuario_id where resp.pregunta_id=${param.id}
</sql:query>

<html lang="en">
  <head>
    <%@ include file="layouts/head.jsp" %>
  </head>

  <body>
        <jsp:include page="layouts/nav.jsp" />
      
      <div class="container">
        <div class="jumbotron">
            <p>${rs.rows[0].texto}</p>
            <p>
                <c:if test="${sessionScope.usuario != null}"><a class="btn btn-primary" href="respuesta?id=${param.id}" role="button">Responder</a></c:if>
                <c:if test="${sessionScope.usuario.id == rs.rows[0].usuario_id}"><a class="btn btn-primary" href="topicos?id=${param.id}" role="button">Asignar T�pico</a></c:if>
            </p>
        </div>
        
        
        <c:forEach var="resp" items="${respuestas.rows}">
            <div class="row">
                <div class="col-md-12">
                    ${resp.texto}
                </div>
            </div>
            <div class="row">
                <div class="col-md-9">
                </div>
                <div class="col-md-3">
                    <b>Respuesta de:</b> ${resp.nombre}
                </div>
            </div>
        </c:forEach>
      </div>
  </body>
</html>