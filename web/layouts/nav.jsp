<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<nav class="navbar navbar-inverse">
<div class="container">
  <div class="navbar-header">
    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
      <span class="sr-only">Toggle navigation</span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
    </button>
    <a class="navbar-brand" href="#">Polipreguntas</a>
  </div>
  <div id="navbar" class="collapse navbar-collapse">
    <ul class="nav navbar-nav">
      <li><a href="index.jsp">Preguntas</a></li>
    </ul>
    <ul class="nav navbar-nav navbar-right">
      <li>
        <c:choose>
            <c:when test="${sessionScope.usuario == null}">
                <a href="iniciar.jsp">Iniciar Sesi�n</a>
            </c:when>
            <c:otherwise>
                <p class="navbar-text">Bienvenido ${sessionScope.usuario.getNombre()} <a href="/electiva3/logout">Cerrar Sesi�n</a></p>
            </c:otherwise>
        </c:choose>
      </li>
    </ul>
  </div><!--/.nav-collapse -->
</div>
</nav>
