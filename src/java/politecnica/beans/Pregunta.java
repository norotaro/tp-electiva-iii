/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package politecnica.beans;

import com.mysql.jdbc.PreparedStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Nelson Otazo
 */
public class Pregunta extends Bean {

    private int id;
    private String texto;
    private int usuario_id;

    public static Pregunta find(int id) {
        Pregunta pregunta = new Pregunta();
        Connection con = _getConnection();
        PreparedStatement stmt;
        ResultSet rs;
        String query = "select id, texto from pregunta where id=?";

        try {
            stmt = (PreparedStatement) con.prepareStatement( query );
            stmt.setInt(1, id);
            rs = stmt.executeQuery();
            rs.first();
            pregunta.setId(rs.getInt(1));
            pregunta.setTexto(rs.getString(1));
        } catch (Exception e) {
            System.out.println(query);
            System.out.println(e);
        }
        
        return pregunta;
    }
    
    public List<Pregunta> findAll() {
        List<Pregunta> preguntas = new ArrayList<Pregunta>();
        Connection con = this.getConnection();
        PreparedStatement stmt;
        ResultSet rs;
        Pregunta pregunta;
        String query = "select id, texto from pregunta";

        try {
            stmt = (PreparedStatement) con.prepareStatement( query );
            rs = stmt.executeQuery();
            
            while (rs.next()) {
                pregunta = new Pregunta();
                pregunta.setId(rs.getInt("id"));
                pregunta.setTexto(rs.getString("texto"));

                preguntas.add(pregunta);
            }
        } catch (Exception e) {
            System.out.println(query);
            System.out.println(e);
        }
        
        return preguntas;
    }
    
    public void asignarTopico(int topico_id) {
        Connection con = this.getConnection();
        PreparedStatement stmt;
        
        try {
            stmt = (PreparedStatement) con.prepareStatement("INSERT INTO topico_pregunta (topico_id, pregunta_id)"
                    + " VALUES (?, ?)" );
            stmt.setInt(1, topico_id);
            stmt.setInt(2, getId());
            stmt.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(Respuesta.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public List<Topico> topicos() {
        List<Topico> topicos = new ArrayList<Topico>();
        Connection con = this.getConnection();
        PreparedStatement stmt;
        ResultSet rs;
        Topico topico;
        
        try {
            stmt = (PreparedStatement) con.prepareStatement(
                    "select id,nombre from topico where id in " + 
                            "(select topico_id from topico_pregunta where pregunta_id = ?)" );
            stmt.setInt(1, this.getId());
            
            rs = stmt.executeQuery();
            
            while (rs.next()) {
                topico = new Topico();
                topico.setId(rs.getInt("id"));
                topico.setNombre(rs.getString("nombre"));
                
                topicos.add(topico);
            }
        } catch (Exception e) {
        }
        
        return topicos;
    }
    
    public int cantRespuestas() {        
        Connection con = this.getConnection();
        PreparedStatement stmt;
        ResultSet rs = null;    
        int canResp = 0;
        
        try {
            stmt = (PreparedStatement) con.prepareStatement("select count(*) from respuesta where pregunta_id = ?" );
            stmt.setInt(1, this.getId());
            
            rs = stmt.executeQuery();
            
            rs.first();                                                     
            canResp = rs.getInt(1);  
        } catch (Exception e) {
            
        }
        
        return canResp;          
    }
    
    public Boolean save() {
        Connection con = this.getConnection();
        PreparedStatement stmt;
        
        try {
            stmt = (PreparedStatement) con.prepareStatement("INSERT INTO pregunta (usuario_id, texto)"
                    + " VALUES (?, ?)" );
            stmt.setInt(1, getUsuario_id());
            stmt.setString(2, getTexto());
            
            stmt.executeUpdate();
            
            stmt = (PreparedStatement) con.prepareStatement("SELECT LAST_INSERT_ID()" );
            ResultSet rs = stmt.executeQuery();
            rs.first();
            
            this.setId(rs.getInt(1));
            
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(Respuesta.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return false;
    }
    
    private static Connection _getConnection() {
        Connection con = null;
        
        try {
            Class.forName("com.mysql.jdbc.Driver");
            con =  DriverManager.getConnection("jdbc:mysql://localhost:3306/electiva3","root","admin");
        } catch( Exception e) {
            System.out.println(e.getMessage());
        }
        
        return con;
    }
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Pregunta() {
    }

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }

    public int getUsuario_id() {
        return usuario_id;
    }

    public void setUsuario_id(int usuario_id) {
        this.usuario_id = usuario_id;
    }
    
    
}
