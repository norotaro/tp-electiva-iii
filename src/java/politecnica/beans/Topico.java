/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package politecnica.beans;

import com.mysql.jdbc.PreparedStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Nelson Otazo
 */
public class Topico extends Bean {
    private int id;
    private String nombre;
    
    public List<Topico> findAll() {
        List<Topico> topicos = new ArrayList<Topico>();
        Connection con = this.getConnection();
        PreparedStatement stmt;
        ResultSet rs;
        Topico topico;
        String query = "select id, nombre from topico";

        try {
            stmt = (PreparedStatement) con.prepareStatement( query );
            rs = stmt.executeQuery();
            
            while (rs.next()) {
                topico = new Topico();
                topico.setId(rs.getInt("id"));
                topico.setNombre(rs.getString("nombre"));

                topicos.add(topico);
            }
        } catch (Exception e) {
            System.out.println(query);
            System.out.println(e);
        }
        
        return topicos;
    }

    public Topico() {
    }
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
    
}
