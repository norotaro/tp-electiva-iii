/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package politecnica.beans;

import com.mysql.jdbc.PreparedStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Nelson Otazo
 */
public class Respuesta extends Bean {
    private int id;
    private String texto;
    private int usuario_id;
    private int pregunta_id;

    public Respuesta() {
    }
    
    public Boolean save() {
        Connection con = this.getConnection();
        PreparedStatement stmt;
        
        try {
            stmt = (PreparedStatement) con.prepareStatement("INSERT INTO respuesta (usuario_id, pregunta_id, texto)"
                    + " VALUES (?, ?, ?)" );
            stmt.setInt(1, getUsuario_id());
            stmt.setInt(2, getPregunta_id());
            stmt.setString(3, getTexto());
            
            stmt.executeUpdate();
            
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(Respuesta.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return false;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }

    public int getUsuario_id() {
        return usuario_id;
    }

    public void setUsuario_id(int usuario_id) {
        this.usuario_id = usuario_id;
    }

    public int getPregunta_id() {
        return pregunta_id;
    }

    public void setPregunta_id(int pregunta_id) {
        this.pregunta_id = pregunta_id;
    }
    
    
}
