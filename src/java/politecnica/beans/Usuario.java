/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package politecnica.beans;


import com.mysql.jdbc.PreparedStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

/**
 *
 * @author Nelson Otazo
 */
public class Usuario extends Bean{
    private Integer id;
    private String nombre;
    private String clave;

    public static Usuario find(Integer id) {
        InitialContext ctx ;
        DataSource  ds;		
        Connection con = null;
        PreparedStatement stmt = null;
        Usuario respuesta = null;
        
        try {
            ctx = new InitialContext();
            ds = (DataSource) ctx.lookup("java:comp/env/jdbc/TestDB");
            try {
                con = (Connection) ds.getConnection();
                stmt = (PreparedStatement) con.prepareStatement("select id, nombre, clave from usuario where id = ?" );
                stmt.setInt(1, id);

                ResultSet rs = stmt.executeQuery();
                rs.first();
                
                respuesta = new Usuario();
                respuesta.setId(rs.getInt("id"));
                respuesta.setNombre(rs.getString("nombre"));
                respuesta.setClave(rs.getString("clave"));
            }  finally {
                    if (stmt != null) { stmt.close(); }
            }
        } catch( Exception e) {
			
	}
        
        return respuesta;
    }

    public static Usuario find(String nombre, String clave) {
        InitialContext ctx ;
        DataSource  ds;		
        Connection con = null;
        PreparedStatement stmt = null;
        Usuario respuesta = null;
        
        try {
            ctx = new InitialContext();
            ds = (DataSource) ctx.lookup("java:comp/env/jdbc/TestDB");
            try {
                con =  DriverManager.getConnection("jdbc:mysql://localhost:3306/electiva3","root","admin");
                stmt = (PreparedStatement) con.prepareStatement("select id, nombre, clave from usuario where nombre like ? and clave like ?" );
                stmt.setString(1, nombre);
                stmt.setString(2, clave);

                ResultSet rs = stmt.executeQuery();
                rs.first();
                
                respuesta = new Usuario();
                respuesta.setId(rs.getInt(1));
                respuesta.setNombre(rs.getString(2));
                respuesta.setClave(rs.getString(3));
                return respuesta;
                /*respuesta.setId(rs.getInt("id"));
                respuesta.setNombre(rs.getString("nombre"));
                respuesta.setClave(rs.getString("clave"));*/
            }  finally {
                    if (stmt != null) { stmt.close(); }
            }
        } catch( Exception e) {
            System.out.println(e.getMessage());
        }
        
        return respuesta;
    }
    
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }
    
    
}
