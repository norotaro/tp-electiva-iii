package politecnica.electiva3;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import politecnica.beans.Usuario;

/**
 *
 * @author Nelson Otazo
 */
public class Login extends HttpServlet {

    //metodo encargado de la gestión del método POST
    protected void processRequestPOST(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
 
        HttpSession sesion = request.getSession();
        Usuario usuario = Usuario.find(request.getParameter("user"), request.getParameter("password"));
        //deberíamos buscar el usuario en la base de datos, pero dado que se escapa de este tema, ponemos un ejemplo en el mismo código
        if(usuario != null){
            //si coincide usuario y password y además no hay sesión iniciada
            sesion.setAttribute("usuario", usuario);
            //redirijo a página con información de login exitoso
            response.sendRedirect("index.jsp");
        }else{
            PrintWriter out = response.getWriter();
            out.println(request.getParameter("user"));
            out.println("<br>");
            out.println(request.getParameter("password"));
            //response.sendRedirect("login/iniciar.jsp?error=true");
        }
    }   
    

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequestPOST(request, response);
    }

}
